package edu.stthomas.seis635.procurement.customersevice;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import akka.actor.ActorRef;
import akka.actor.ReceiveTimeout;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import edu.stthomas.seis635.common.message.CustomerMessage.CustomerOrdersResult;
import edu.stthomas.seis635.common.message.ProcurementMessages.VendorBidOffer;
import scala.concurrent.duration.Duration;

public class CustomerServiceResult extends UntypedActor {

	private LoggingAdapter log = Logging.getLogger(getContext().system(), this);
	private final int expectedResults;
	private final ActorRef replyTo;
	private final ActorRef customerBid;
	private List<VendorBidOffer> bidOffers;
	private final String sortBy;
	private int receivedResults;

	public CustomerServiceResult(int expectedResults, String sortBy, ActorRef replyTo, ActorRef customerBid) {
		this.expectedResults = expectedResults;
		this.customerBid = customerBid;
		this.sortBy = sortBy;
		this.bidOffers = new ArrayList<>();
		this.replyTo = replyTo;
		this.receivedResults = 0;
	}

	@Override
	public void preStart() {
		getContext().setReceiveTimeout(Duration.create(3, TimeUnit.SECONDS));
	}

	@Override
	public void onReceive(Object message) throws Exception {
		log.info("{} received message of type {}", this.getClass().getName(), message.getClass().getName());
		if (message instanceof VendorBidOffer) {
			receivedResults += 1;
			bidOffers.add((VendorBidOffer) message);
			log.info("Expected results {}, Current results count {}", expectedResults, receivedResults);
			if (expectedResults == receivedResults) {
				log.info("Preparing to send reply to {}", replyTo.path());
				// find best bid based on customer selection criteria
				VendorBidOffer bo = getCustomerVendorBidOffer();
				replyTo.tell(bo, getSelf());
				customerBid.tell(bo, ActorRef.noSender());
				getContext().stop(getSelf());
			}
		} else if (message instanceof CustomerOrdersResult) {
			replyTo.tell(message, getSelf());
		} else if (message == ReceiveTimeout.getInstance()) {
			log.warning("Did not receive enough bid offers. Sending the best offer received");
			log.info("Preparing to send reply to {}", replyTo.path());
			// find best bid based on customer selection criteria
			VendorBidOffer bo = getCustomerVendorBidOffer();
			replyTo.tell(bo, getSelf());
			customerBid.tell(bo, ActorRef.noSender());
			getContext().stop(getSelf());

		} else {
			log.info("{} could not handle message of type {}. Message: ", this.getClass().getName(),
					message.getClass().getName(), message);
			unhandled(message);
		}
	}

	private VendorBidOffer getCustomerVendorBidOffer() {
		if (bidOffers != null) {
			if (sortBy.equalsIgnoreCase("price")) {
				Collections.sort(bidOffers, (l, r) -> l.getProduct().getPrice().compareTo(r.getProduct().getPrice()));
				log.info("Sorted by price bid offers {}", bidOffers.toString());
			} else {
				Collections.sort(bidOffers,
						(l, r) -> l.getProduct().getTimeToShipInDays().compareTo(r.getProduct().getTimeToShipInDays()));
			}
			return bidOffers.get(0);
		}
		return null;
	}
}
