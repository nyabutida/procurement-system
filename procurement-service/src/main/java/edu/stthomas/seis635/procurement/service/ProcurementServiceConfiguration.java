package edu.stthomas.seis635.procurement.service;

import static edu.stthomas.seis635.common.injection.SpringExtension.SpringExtProvider;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorSystem;

@Configuration
public class ProcurementServiceConfiguration {
	private static final Logger logger = LoggerFactory
			.getLogger(ProcurementServiceConfiguration.class);
	@Autowired
	private ApplicationContext applicationContext;
	@Value("${tribe.router.conf}")
	private String router;
	@Value("$[akka.application.conf}")
	private String applicationConf;

	/**
	 * system singleton
	 */
	@Bean(name = { "procurementServiceSystem" })
	public ActorSystem actorSystem() {
		// initialize the application context in the Akka Spring Extension
		System.out.println("Router Path "+router);
		System.out.println("File Exists "+(new File(router)).exists());
		String role = (System.getProperties().getProperty("role"));
		if(role == null || role.length()==0)
			role = "procurementService";
		Config config = ConfigFactory
				.parseString("akka.remote.netty.tcp.port=" + System.getProperties().getProperty("servicePort"))
				.withFallback(ConfigFactory.parseString("akka.cluster.roles = ["+role+"]"))		
				.withFallback(ConfigFactory.parseFile(new File(router)).resolveWith(ConfigFactory.parseString("servicePort = "+System.getProperties().getProperty("servicePort"))))
				.withFallback(ConfigFactory.parseFile(new File(applicationConf)).resolve());
		logger.info("Initializaing Procurement Service System...");
		ActorSystem system = ActorSystem.create("ClusterSystem", config);
		logger.info("Setting procurement service system application context...");
		SpringExtProvider.get(system).initialize(applicationContext);
		return system;
	}

}
