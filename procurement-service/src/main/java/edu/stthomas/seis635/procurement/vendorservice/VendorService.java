package edu.stthomas.seis635.procurement.vendorservice;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import edu.stthomas.seis635.common.message.VendorMessage.AddProduct;
import edu.stthomas.seis635.common.message.VendorMessage.GetProduct;
import edu.stthomas.seis635.common.message.VendorMessage.ListInventory;

public class VendorService  extends UntypedActor {
	private LoggingAdapter log = Logging.getLogger(getContext().system(), this);
	private ActorRef vendorInventory;

	public VendorService(ActorRef vendorInventory){
		this.vendorInventory = vendorInventory;
	}
	@Override
	public void onReceive(Object message) throws Exception {
		log.info("{} received message of type {}",this.getClass().getName(), message.getClass().getName());
		if(message instanceof AddProduct){
			vendorInventory.tell(message, getSender());
		}else if(message instanceof ListInventory){
			vendorInventory.tell(message, getSender());
		}else if(message instanceof GetProduct){
			vendorInventory.tell(message, getSender());
		}else {
			log.info("{} could not handle message of type {}. Message: ",this.getClass().getName(),message.getClass().getName(), message);
			unhandled(message);
		}
		
	}

}
