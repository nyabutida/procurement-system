package edu.stthomas.seis635.procurement.vendorservice;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import edu.stthomas.seis635.common.message.VendorMessage;
import edu.stthomas.seis635.common.message.ProcurementMessages.VendorBidOffer;
import edu.stthomas.seis635.common.vendor.VendorInventoryList;

public class VendorServiceResult extends UntypedActor {

	private LoggingAdapter log = Logging.getLogger(getContext().system(), this);
	private final int expectedResults;
	private final ActorRef replyTo;
	private int receivedResults;

	public VendorServiceResult(int expectedResults, ActorRef replyTo) {
		this.expectedResults = expectedResults;
		this.replyTo = replyTo;
		this.receivedResults = 0;
	}

	@Override
	public void onReceive(Object message) throws Exception {
		log.info("{} received message of type {}", this.getClass().getName(), message.getClass().getName());
		
		if(message instanceof VendorMessage.AddProductResult){
			receivedResults+=1;
			if(expectedResults == receivedResults){
				replyTo.tell(message, getSelf());
			}
		}else if(message instanceof VendorInventoryList){
			receivedResults+=1;
			if(expectedResults == receivedResults){
				replyTo.tell(message, getSelf());
			}
		}else if(message instanceof VendorBidOffer){
			receivedResults+=1;
			log.info("Expected results {}, Current results count {}",expectedResults,receivedResults);
			if(expectedResults == receivedResults){
				replyTo.tell(message, getSelf());
			}
		}else {
			log.info("{} could not handle message of type {}. Message: ",this.getClass().getName(),message.getClass().getName(), message);
			unhandled(message);
		}
	}

}
