package edu.stthomas.seis635.procurement.customersevice;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.cluster.pubsub.DistributedPubSub;
import akka.cluster.pubsub.DistributedPubSubMediator;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import edu.stthomas.seis635.common.message.CustomerMessage.GetBid;
import edu.stthomas.seis635.common.message.CustomerMessage.ListCustomerOrders;
import edu.stthomas.seis635.common.message.VendorMessage.GetProduct;

public class CustomerService extends UntypedActor{
	private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);
	private final ActorRef customerBid;
	private final ActorRef mediator =
		    DistributedPubSub.get(getContext().system()).mediator();
	
	public CustomerService(ActorRef customerBid){
		this.customerBid = customerBid;
	}
	
	@Override
	public void onReceive(Object message) throws Exception {
		log.info("{} received message of type {} ",this.getClass().getName(),message.getClass().getName());
		ActorRef replyTo = getSender();
		if(message instanceof GetBid){
			GetBid msg = (GetBid)message;
			GetProduct product = new GetProduct(msg.getProductId(),msg.getCustomerId());
			//set CustomerServiceResult as sender and broadcast message to bid topic
			log.info("After processing reply should be sent to {}",replyTo.path());
			 mediator.tell(new DistributedPubSubMediator.Publish("bid", product),
					 replyTo);
		}else if(message instanceof ListCustomerOrders){
			log.info("Forwarding message to customer Bid Actor");
			customerBid.tell(message, replyTo);
		}else {
			log.info("{} could not handle message of type {}. Message: ",this.getClass().getName(),message.getClass().getName(), message);
			unhandled(message);
		}		
	}
}
