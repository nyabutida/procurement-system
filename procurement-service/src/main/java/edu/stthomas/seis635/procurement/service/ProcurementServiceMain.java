package edu.stthomas.seis635.procurement.service;

import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.StringUtils;

import akka.actor.ActorSystem;
import akka.actor.Props;

public class ProcurementServiceMain {
	private static final Logger logger = LoggerFactory.getLogger(ProcurementServiceMain.class);
	private static ApplicationContext ctx;
	
	public static void main(String[] args){
		  ctx = new ClassPathXmlApplicationContext(
					"application-context.xml");
		  logger.info("Initialize Proceurement Service ActorSystem...");
		  ActorSystem system = (ActorSystem) ctx.getBean("procurementServiceSystem");
			Properties prop = (Properties)ctx.getBean("applicationConfig");
		  logger.info("Initialize ProcurementService Actor...");
		  String servicePath = System.getProperties().getProperty("service.path");
		  if(StringUtils.isEmpty(servicePath)){
			  servicePath=ProcurementSeviceConstants.DEFAULT_SERVICE_PATH;
		  }
		  system.actorOf(Props.create(ProcurementService.class,prop),
					"procurementservice");
	  }
}
