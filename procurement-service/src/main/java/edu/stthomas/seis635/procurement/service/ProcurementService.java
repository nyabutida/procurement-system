package edu.stthomas.seis635.procurement.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.cluster.pubsub.DistributedPubSub;
import akka.cluster.pubsub.DistributedPubSubMediator;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import akka.routing.ActorRefRoutee;
import akka.routing.RoundRobinRoutingLogic;
import akka.routing.Routee;
import akka.routing.Router;
import edu.stthomas.seis635.common.message.CustomerMessage;
import edu.stthomas.seis635.common.message.CustomerMessage.AcceptBid;
import edu.stthomas.seis635.common.message.CustomerMessage.GetBid;
import edu.stthomas.seis635.common.message.CustomerMessage.ListCustomerOrders;
import edu.stthomas.seis635.common.message.ProcurementMessages;
import edu.stthomas.seis635.common.message.VendorMessage;
import edu.stthomas.seis635.procurement.customersevice.CustomerBid;
import edu.stthomas.seis635.procurement.customersevice.CustomerServiceResult;
import edu.stthomas.seis635.procurement.vendorservice.VendorInventory;
import edu.stthomas.seis635.procurement.vendorservice.VendorServiceResult;

public class ProcurementService extends UntypedActor {
	private LoggingAdapter log = Logging.getLogger(getContext().system(), this);
	private Router router;
	private final ActorRef vendorInventory;
	private final ActorRef customerBid;

	public ProcurementService(Properties properties) {
		ActorRef mediator = DistributedPubSub.get(getContext().system()).mediator();
		// subscribe to the topic named "content"
		mediator.tell(new DistributedPubSubMediator.Subscribe("bid", getSelf()), getSelf());
		this.vendorInventory = getContext().actorOf(Props.create(VendorInventory.class));
		this.customerBid = getContext().actorOf(Props.create(CustomerBid.class));
		List<Routee> routees = new ArrayList<Routee>();
		for (int i = 0; i < 5; i++) {
			ActorRef r = getContext()
					.actorOf(Props.create(ProcurementServiceWorker.class, vendorInventory, customerBid));
			getContext().watch(r);
			routees.add(new ActorRefRoutee(r));
		}
		router = new Router(new RoundRobinRoutingLogic(), routees);
	}

	@Override
	public void onReceive(Object message) throws Exception {
		final ActorRef replyTo = getSender();

		log.info("{} received message of type {}", this.getClass().getName(), message.getClass().getName());
		if (message instanceof ProcurementMessages.Identify) {
			log.info("Procurement Service Received {}", message.getClass().getName());
			String vendorId = System.getProperties().getProperty("vendorId");
			log.info("My Vendor ID is {}", vendorId);
			getSender().tell(
					new ProcurementMessages.Identity(vendorId, ((ProcurementMessages.Identify) message).getAddress()),
					getSelf());
		} else if (message instanceof VendorMessage) {
			VendorMessage msg = (VendorMessage) message;
			// create actor that collects replies from workers
			ActorRef supervisor = getContext().actorOf(Props.create(VendorServiceResult.class, 1, replyTo));
			router.route(msg, supervisor);

		} else if (message instanceof CustomerMessage) {
			if (message instanceof GetBid) {
				GetBid msg = (GetBid) message;
				ActorRef supervisor = getContext().actorOf(
						Props.create(CustomerServiceResult.class, msg.getMinNumberOfBids(), msg.getSortBy(), replyTo,customerBid));
				router.route(msg, supervisor);
			} else if (message instanceof AcceptBid){
				vendorInventory.tell(message, replyTo);
			} else if (message instanceof ListCustomerOrders){
				customerBid.tell(message, replyTo);
			}else {
				VendorMessage msg = (VendorMessage) message;
				// create actor that collects replies from workers
				ActorRef supervisor = getContext().actorOf(Props.create(CustomerServiceResult.class, 1, replyTo,customerBid));
				router.route(msg, supervisor);
			}

		} else if (message instanceof DistributedPubSubMediator.SubscribeAck) {
			log.info("subscribing to bid events");
		} else {
			log.info("{} could not handle message of type {}. Message: {}", this.getClass().getName(),
					message.getClass().getName(), message);
			unhandled(message);
		}
	}

}
