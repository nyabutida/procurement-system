package edu.stthomas.seis635.procurement.vendorservice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.cluster.pubsub.DistributedPubSub;
import akka.cluster.pubsub.DistributedPubSubMediator;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import edu.stthomas.seis635.common.message.CustomerMessage.AcceptBid;
import edu.stthomas.seis635.common.message.ProcurementMessages.VendorBidOffer;
import edu.stthomas.seis635.common.message.VendorMessage.AddProduct;
import edu.stthomas.seis635.common.message.VendorMessage.AddProductResult;
import edu.stthomas.seis635.common.message.VendorMessage.GetProduct;
import edu.stthomas.seis635.common.message.VendorMessage.ListInventory;
import edu.stthomas.seis635.common.vendor.Product;
import edu.stthomas.seis635.common.vendor.VendorInventoryList;

public class VendorInventory extends UntypedActor {

	private LoggingAdapter log = Logging.getLogger(getContext().system(), this);
	private final Map<String,List<Product>> products = new HashMap<String,List<Product>>();
	private final Map<String,Product> pendingOffers = new HashMap<String,Product>();
	private final ActorRef mediator =
		    DistributedPubSub.get(getContext().system()).mediator();
	
	@Override
	public void onReceive(Object message) throws Exception {
		log.info("{} received message of type {}",this.getClass().getName(), message.getClass().getName());
		log.info("Size of inventory before processing request is {}",products.size() );
		if(message instanceof AddProduct){
			AddProduct msg = (AddProduct)message;
			AddProductResult result = AddProductToInventory(msg);
			getSender().tell(result, getSelf());
			
		}else if(message instanceof ListInventory){ 
			ListInventory msg = (ListInventory)message;
			log.info("Inventory size {}",products.size());
			VendorInventoryList inventoryList = new VendorInventoryList(msg.getVendorId(), products);
			log.info("Inventory {}", inventoryList.toString());
			getSender().tell(inventoryList, getSelf());
		}else if(message instanceof GetProduct){
			GetProduct msg = (GetProduct)message;
			if(products.containsKey(msg.getProductId())){
				String bidOfferId = System.getProperties().getProperty("vendorId")+"_"+System.currentTimeMillis();
				VendorBidOffer vbo = new VendorBidOffer(System.getProperties().getProperty("vendorId"), products.get(msg.getProductId()).get(0), bidOfferId, msg.getCustomerId());
				pendingOffers.put(bidOfferId, products.get(msg.getProductId()).remove(0));
				getSender().tell(vbo, getSelf());
				//broadcast message
				mediator.tell(new DistributedPubSubMediator.Publish("acceptBid", vbo),
						 getSender());
				log.info("Processed GetProduct message");
			}
		}else if (message instanceof AcceptBid){
			AcceptBid msg = (AcceptBid)message;
			if(pendingOffers.size() > 0 && pendingOffers.containsKey(msg.getBidOfferId())){
				pendingOffers.remove(msg.getBidOfferId());
				//broadcast message
				mediator.tell(new DistributedPubSubMediator.Publish("acceptBid", message),
						 getSender());
			}else{
				log.warning("This is not the origin of the bid offer {}",msg.getBidOfferId());
			}
		}else {
			log.info("{} could not handle message of type {}. Message: ",this.getClass().getName(),message.getClass().getName(), message);
			unhandled(message);
		}
		log.info("Size of inventory after processing request is {}",products.size() );
	}
	
	private AddProductResult AddProductToInventory(AddProduct addProduct){
		if(!products.containsKey(addProduct.getProductId())){
			List<Product> productList = new ArrayList<Product>();
			products.put(addProduct.getProductId(),productList);
		}
		log.info("Adding the following product to vendor id {}'s inventory. {}",addProduct.getVendorId(),addProduct.toProduct());
		boolean isProcessed = products.get(addProduct.getProductId()).add(addProduct.toProduct());
		log.info("The request was processed [True/False]? {}",isProcessed);
		return new AddProductResult(addProduct.getProductId(), addProduct.getVendorId(), isProcessed);
	}
	
}
