package edu.stthomas.seis635.procurement.customersevice;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.cluster.pubsub.DistributedPubSub;
import akka.cluster.pubsub.DistributedPubSubMediator;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import edu.stthomas.seis635.common.customer.CustomerOrders;
import edu.stthomas.seis635.common.message.CustomerMessage.AcceptBid;
import edu.stthomas.seis635.common.message.CustomerMessage.ListCustomerOrders;
import edu.stthomas.seis635.common.message.ProcurementMessages.VendorBidOffer;
import edu.stthomas.seis635.common.vendor.Product;

public class CustomerBid extends UntypedActor {

	private LoggingAdapter log = Logging.getLogger(getContext().system(), this);
	private final Map<String, Map<String, List<Product>>> customerOrders = new HashMap<>();
	private final Map<String, Product> incompleteOrders = new HashMap<>();

	public CustomerBid() {
		ActorRef mediator = DistributedPubSub.get(getContext().system()).mediator();
		// subscribe to the topic named "content"
		mediator.tell(new DistributedPubSubMediator.Subscribe("acceptBid", getSelf()), getSelf());
	}

	@Override
	public void onReceive(Object message) throws Exception {

		log.info("{} received message of type {} ", this.getClass().getName(), message.getClass().getName());
		if (message instanceof ListCustomerOrders) {
			ListCustomerOrders msg = (ListCustomerOrders) message;
			CustomerOrders co = null;
			if (customerOrders.containsKey(msg.getCustomerId())) {
				log.info("Order size {}", customerOrders.get(msg.getCustomerId()).size());
				co = new CustomerOrders(msg.getCustomerId(), customerOrders.get(msg.getCustomerId()));
				getSender().tell(co, getSelf());
			} else {
				log.info("Order size {}", 0);
				co = new CustomerOrders(msg.getCustomerId(), new HashMap<String, List<Product>>());
				log.info("The customer has no orders...");

			}
			getSender().tell(co, getSelf());

		} else if (message instanceof VendorBidOffer) {
			VendorBidOffer msg = (VendorBidOffer) message;
			incompleteOrders.put(msg.getBidOfferId(), msg.getProduct());
		} else if (message instanceof AcceptBid) {
			AcceptBid msg = (AcceptBid) message;
			if (incompleteOrders.containsKey(msg.getBidOfferId())) {
				if (!customerOrders.containsKey(msg.getCustomerId())) {
					Map<String, List<Product>> cp = new HashMap<>();
					List<Product> productList = new ArrayList<Product>();
					cp.put(incompleteOrders.get(msg.getBidOfferId()).getProductId(), productList);
					customerOrders.put(msg.getCustomerId(), cp);
				}

				log.info("Adding the following product to customer id {}'s orders. {}", msg.getCustomerId(),
						incompleteOrders.get(msg.getBidOfferId()));
				boolean isProcessed = customerOrders.get(msg.getCustomerId())
						.get((incompleteOrders.get(msg.getBidOfferId()).getProductId()))
						.add(incompleteOrders.remove(msg.getBidOfferId()));
				log.info("The request was processed [True/False]? {}", isProcessed);
			} else {
				log.warning("The bid offer id {} has either been accepted or does not exist", msg.getBidOfferId());
			}
		}else if (message instanceof ListCustomerOrders){
			ListCustomerOrders msg = (ListCustomerOrders)message;
			if(customerOrders.containsKey(msg.getCustomerId())){
				CustomerOrders co = new CustomerOrders(msg.getCustomerId(), customerOrders.get(msg.getCustomerId()));
				getSender().tell(co, getSelf());
			}else {
				//TODO Customer has no orders
			}
		} else if (message instanceof DistributedPubSubMediator.SubscribeAck) {
			log.info("subscribing to accept bid events");
		} else {
			log.info("{} could not handle message of type {}. Message: ", this.getClass().getName(),
					message.getClass().getName(), message);
			unhandled(message);
		}
	}

}
