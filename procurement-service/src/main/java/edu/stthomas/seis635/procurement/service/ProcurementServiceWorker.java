package edu.stthomas.seis635.procurement.service;

import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import edu.stthomas.seis635.common.message.CustomerMessage;
import edu.stthomas.seis635.common.message.VendorMessage;
import edu.stthomas.seis635.procurement.customersevice.CustomerService;
import edu.stthomas.seis635.procurement.vendorservice.VendorService;

public class ProcurementServiceWorker extends UntypedActor {
	private final LoggingAdapter log = Logging.getLogger(getContext().system(), this);

	private final ActorRef vendorInventory;
	private final ActorRef customerBid;

	public ProcurementServiceWorker(ActorRef vendorInventory, ActorRef customerBid) {
		this.vendorInventory = vendorInventory;
		this.customerBid = customerBid;
	}

	@Override
	public void onReceive(Object message) throws Exception {
		final ActorRef replyTo = getSender();
		log.info("{} received message of type {} ",this.getClass().getName(),message.getClass().getName());
		if (message instanceof VendorMessage) {
			ActorRef service = getContext().actorOf(
					Props.create(VendorService.class, vendorInventory));
			service.tell(message, replyTo);
		}else if(message instanceof CustomerMessage){
			ActorRef service = getContext().actorOf(
					Props.create(CustomerService.class, customerBid));
			service.tell(message, replyTo);
		} else {
			log.info("{} could not handle message of type {}. Message: ",this.getClass().getName(),message.getClass().getName(), message);
			unhandled(message);
		}

	}

}
