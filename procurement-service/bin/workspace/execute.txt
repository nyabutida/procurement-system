#Run on local
#Admin
java -cp /home/davis/procurement-service/procurement-service.jar  \
-DlogFilePath=/home/davis/procurement-service/config/log/procurement-service.log  \
-Dlog4j.configuration=file:///home/davis/procurement-service/config/log4j.xml  \
-DconfigFilePath=/home/davis/procurement-service/config  \
-Dconfig.file=/home/davis/procurement-service/config/router.conf   \
-DservicePort=2551  \
-Drole=admin \
edu.stthomas.seis635.procurement.service.ProcurementServiceMain

#Vendor
java -cp /home/davis/procurement-service/procurement-service.jar  \
-DlogFilePath=/home/davis/procurement-service/config/log/procurement-service.log  \
-Dlog4j.configuration=file:///home/davis/procurement-service/config/log4j.xml  \
-DconfigFilePath=/home/davis/procurement-service/config  \
-Dconfig.file=/home/davis/procurement-service/config/router.conf \
-DservicePort=0  \
-DvendorId=001 \
edu.stthomas.seis635.procurement.service.ProcurementServiceMain

java -cp /home/davis/procurement-service/procurement-service.jar  \
-DlogFilePath=/home/davis/procurement-service/config/log/procurement-service.log  \
-Dlog4j.configuration=file:///home/davis/procurement-service/config/log4j.xml  \
-DconfigFilePath=/home/davis/procurement-service/config  \
-Dconfig.file=/home/davis/procurement-service/config/router.conf \
-DservicePort=0  \
-DvendorId=002 \
edu.stthomas.seis635.procurement.service.ProcurementServiceMain


java -cp /home/davis/procurement-service/procurement-service.jar  \
-DlogFilePath=/home/davis/procurement-service/config/log/procurement-service.log  \
-Dlog4j.configuration=file:///home/davis/procurement-service/config/log4j.xml  \
-DconfigFilePath=/home/davis/procurement-service/config  \
-Dconfig.file=/home/davis/procurement-service/config/router.conf \
-DservicePort=0  \
-DvendorId=003 \
edu.stthomas.seis635.procurement.service.ProcurementServiceMain

#Build
gradle clean build -PversionId=dev copyResources -x test