@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  procurement-service startup script for Windows
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%..

@rem Add default JVM options here. You can also use JAVA_OPTS and PROCUREMENT_SERVICE_OPTS to pass JVM options to this script.
set DEFAULT_JVM_OPTS=

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if "%ERRORLEVEL%" == "0" goto init

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto init

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:init
@rem Get command-line arguments, handling Windows variants

if not "%OS%" == "Windows_NT" goto win9xME_args
if "%@eval[2+2]" == "4" goto 4NT_args

:win9xME_args
@rem Slurp the command line arguments.
set CMD_LINE_ARGS=
set _SKIP=2

:win9xME_args_slurp
if "x%~1" == "x" goto execute

set CMD_LINE_ARGS=%*
goto execute

:4NT_args
@rem Get arguments from the 4NT Shell from JP Software
set CMD_LINE_ARGS=%$

:execute
@rem Setup the command line

set CLASSPATH=%APP_HOME%\lib\procurement-service-dev.jar;%APP_HOME%\lib\commons-collections-3.2.jar;%APP_HOME%\lib\procurement-common-dev.jar;%APP_HOME%\lib\akka-actor_2.11-2.4.1.jar;%APP_HOME%\lib\scala-library-2.11.7.jar;%APP_HOME%\lib\akka-remote_2.11-2.4.1.jar;%APP_HOME%\lib\akka-cluster_2.11-2.4.1.jar;%APP_HOME%\lib\akka-cluster-metrics_2.11-2.4.1.jar;%APP_HOME%\lib\akka-contrib_2.11-2.4.1.jar;%APP_HOME%\lib\akka-slf4j_2.11-2.4.1.jar;%APP_HOME%\lib\spring-context-4.1.3.RELEASE.jar;%APP_HOME%\lib\guice-4.0.jar;%APP_HOME%\lib\jackson-core-2.6.4.jar;%APP_HOME%\lib\jackson-annotations-2.6.4.jar;%APP_HOME%\lib\jackson-databind-2.6.4.jar;%APP_HOME%\lib\gson-2.6.1.jar;%APP_HOME%\lib\slf4j-log4j12-1.7.13.jar;%APP_HOME%\lib\slf4j-api-1.7.13.jar;%APP_HOME%\lib\aws-java-sdk-ec2-1.10.47.jar;%APP_HOME%\lib\aws-java-sdk-sqs-1.10.47.jar;%APP_HOME%\lib\httpclient-4.5.2.jar;%APP_HOME%\lib\commons-io-2.4.jar;%APP_HOME%\lib\commons-compress-1.11.jar;%APP_HOME%\lib\config-1.3.0.jar;%APP_HOME%\lib\akka-protobuf_2.11-2.4.1.jar;%APP_HOME%\lib\netty-3.10.3.Final.jar;%APP_HOME%\lib\uncommons-maths-1.2.2a.jar;%APP_HOME%\lib\akka-cluster-tools_2.11-2.4.1.jar;%APP_HOME%\lib\akka-persistence_2.11-2.4.1.jar;%APP_HOME%\lib\spring-aop-4.1.3.RELEASE.jar;%APP_HOME%\lib\spring-beans-4.1.3.RELEASE.jar;%APP_HOME%\lib\spring-core-4.1.3.RELEASE.jar;%APP_HOME%\lib\spring-expression-4.1.3.RELEASE.jar;%APP_HOME%\lib\javax.inject-1.jar;%APP_HOME%\lib\aopalliance-1.0.jar;%APP_HOME%\lib\guava-16.0.1.jar;%APP_HOME%\lib\log4j-1.2.17.jar;%APP_HOME%\lib\aws-java-sdk-core-1.10.47.jar;%APP_HOME%\lib\httpcore-4.4.4.jar;%APP_HOME%\lib\commons-logging-1.2.jar;%APP_HOME%\lib\commons-codec-1.9.jar;%APP_HOME%\lib\joda-time-2.8.1.jar

@rem Execute procurement-service
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %PROCUREMENT_SERVICE_OPTS%  -classpath "%CLASSPATH%" edu.stthomas.seis635.procurement.service.ProcumentServiceMain %CMD_LINE_ARGS%

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable PROCUREMENT_SERVICE_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%PROCUREMENT_SERVICE_EXIT_CONSOLE%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
