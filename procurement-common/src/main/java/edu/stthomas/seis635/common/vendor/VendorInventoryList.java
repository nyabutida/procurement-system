package edu.stthomas.seis635.common.vendor;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class VendorInventoryList implements Serializable{
	
	private static final long serialVersionUID = 2736050382048354205L;
	private final String vendorId;
	private final Map<String,List<Product>> products;

	public VendorInventoryList(String vendorId, Map<String,List<Product>> products) {
		super();
		this.vendorId = vendorId;
		this.products = products;
	}
	
	public String getVendorId() {
		return vendorId;
	}
	
	public Map<String, List<Product>> getProducts() {
		return products;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("The vendor with Id "+vendorId+" has the following products in their inventory \n");
		sb.append("\n");
		products.forEach((k,v)->{
			sb.append("Product Name:"+k+", Quantity: "+v.size());
			sb.append("\n");
		});
		return sb.toString();
	}
	
}
