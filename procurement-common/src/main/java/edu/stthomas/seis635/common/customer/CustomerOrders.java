package edu.stthomas.seis635.common.customer;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import edu.stthomas.seis635.common.vendor.Product;

public class CustomerOrders implements Serializable{
	
	private static final long serialVersionUID = 2736050382048354205L;
	private final String customerId;
	private final Map<String,List<Product>> products;

	public CustomerOrders(String customerId, Map<String,List<Product>> products) {
		super();
		this.customerId = customerId;
		this.products = products;
	}
	
	public String getCustomerId() {
		return customerId;
	}
	
	public Map<String, List<Product>> getProducts() {
		return products;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("The customer with Id "+customerId+" has the following orders \n");
		sb.append("\n");
		products.forEach((k,v)->{
			sb.append("Product Name:"+k+", Quantity: "+v.size());
			sb.append("\n");
		});
		return sb.toString();
	}
	
}
