package edu.stthomas.seis635.common.vendor;

import java.io.Serializable;

public class Product implements Serializable{

	private static final long serialVersionUID = -798004620963130146L;
	private final String name;
	private final String productId;
	private final String description;
	private final String condition;
	private final Double price;
	private final String timeToShipInDays;
	
	public Product(String name, String productId, String description, String condition, Double price,
			String timeToShipInDays) {
		this.name = name;
		this.productId = productId;
		this.description = description;
		this.condition = condition;
		this.price = price;
		this.timeToShipInDays = timeToShipInDays;
	}

	public String getName() {
		return name;
	}

	public String getProductId() {
		return productId;
	}

	public String getDescription() {
		return description;
	}

	public String getCondition() {
		return condition;
	}

	public Double getPrice() {
		return price;
	}

	public String getTimeToShipInDays() {
		return timeToShipInDays;
	}

	@Override
	public String toString() {
		return "Product [name=" + name + ", productId=" + productId + ", description=" + description + ", condition="
				+ condition + ", price=" + price + ", timeToShipInDays=" + timeToShipInDays + "]";
	}
	
}
