package edu.stthomas.seis635.common.message;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import edu.stthomas.seis635.common.vendor.Product;
import edu.stthomas.seis635.common.message.ProcurementMessages.ProcurementMessage;

//immutable message objects for the vendor
public abstract class VendorMessage extends ProcurementMessage implements Serializable {

	private static final long serialVersionUID = 131207807141397372L;
	@JsonProperty("vendorId")
	private String vendorId;

	public String getVendorId() {
		return vendorId;
	}

	@Override
	public String toString() {
		return "VendorMessage [vendorId=" + vendorId + ", toString()=" + super.toString() + "]";
	}

	@JsonTypeName("addVendor")
	public static class AddVendor extends VendorMessage implements Serializable {

		private static final long serialVersionUID = -4375394146636495804L;
		@JsonProperty("vendorName")
		private String vendorName;
		@JsonProperty("vendorRating")
		private String vendorRating;

		public String getVendorName() {
			return vendorName;
		}

		public String getVendorRating() {
			return vendorRating;
		}

		@Override
		public String toString() {
			return "AddVendor [vendorName=" + vendorName + ", vendorRating=" + vendorRating + ", toString()="
					+ super.toString() + "]";
		}

	}

	@JsonTypeName("addProduct")
	public static class AddProduct extends VendorMessage implements Serializable {

		private static final long serialVersionUID = -6886236059887884311L;

		@JsonProperty("name")
		private String name;
		@JsonProperty("productId")
		private String productId;
		@JsonProperty("description")
		private String description;
		@JsonProperty("condition")
		private String condition;
		@JsonProperty("price")
		private Double price;
		@JsonProperty("timeToShipInDays")
		private String timeToShipInDays;

		public String getName() {
			return name;
		}

		public String getProductId() {
			return productId;
		}

		public String getDescription() {
			return description;
		}

		public String getCondition() {
			return condition;
		}

		public Double getPrice() {
			return price;
		}

		public String getTimeToShipInDays() {
			return timeToShipInDays;
		}

		public Product toProduct() {
			return new Product(name, productId, description, condition, price, timeToShipInDays);
		}

		@Override
		public String toString() {
			return "AddProduct [name=" + name + ", productId=" + productId + ", description=" + description
					+ ", condition=" + condition + ", price=" + price + ", timeToShipInDays=" + timeToShipInDays + "]";
		}
	}

	@JsonTypeName("listInventory")
	public static class ListInventory extends VendorMessage implements Serializable {

		private static final long serialVersionUID = -6886236059887884321L;

	}

	public static class AddProductResult implements Serializable {

		private static final long serialVersionUID = -6248503981078393485L;
		private final String id;
		private final String vendorId;
		private final boolean isProcessed;

		public AddProductResult(String id, String vendorId, boolean isProcessed) {
			this.id = id;
			this.vendorId = vendorId;
			this.isProcessed = isProcessed;
		}

		public String getId() {
			return id;
		}

		public String getVendorId() {
			return vendorId;
		}

		public boolean isProcessed() {
			return isProcessed;
		}

		@Override
		public String toString() {
			return "AddProductResult [id=" + id + ", vendorId=" + vendorId + ", isProcessed=" + isProcessed + "]";
		}

	}

	public static class GetProduct extends VendorMessage implements Serializable {

		private static final long serialVersionUID = -6248513981078393485L;
		private final String productId;
		private final String customerId;

		public GetProduct(String productId, String customerId) {
			this.productId = productId;
			this.customerId = customerId;
		}
		public String getProductId() {
			return productId;
		}
		public String getCustomerId() {
			return customerId;
		}
		@Override
		public String toString() {
			return "GetProduct [productId=" + productId + ", customerId=" + customerId + "]";
		}
	}
}
