package edu.stthomas.seis635.common.message;

public interface Traceable {
	String getId();
	void setId(String id);
}
