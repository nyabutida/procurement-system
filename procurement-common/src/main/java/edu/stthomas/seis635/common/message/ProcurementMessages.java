package edu.stthomas.seis635.common.message;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import akka.actor.Address;
import edu.stthomas.seis635.common.vendor.Product;

//#messages

/**
 * @author team 7
 *
 */
public class ProcurementMessages implements Serializable{
	private static final long serialVersionUID = 3964172433750598971L;
	public ProcurementMessage message;

    @JsonTypeInfo(use = JsonTypeInfo.Id.NAME,
                  include = As.PROPERTY,
                  property = "type")
    @JsonSubTypes({
        @JsonSubTypes.Type(value = VendorMessage.AddVendor.class, name = "addVendor"),
        @JsonSubTypes.Type(value = VendorMessage.AddProduct.class, name = "addProduct"),
        @JsonSubTypes.Type(value = VendorMessage.ListInventory.class, name = "listInventory"),
        @JsonSubTypes.Type(value = CustomerMessage.AddCustomer.class, name = "addCustomer"),
        @JsonSubTypes.Type(value = CustomerMessage.GetBid.class, name = "getBid"),
        @JsonSubTypes.Type(value = CustomerMessage.AcceptBid.class, name = "acceptBid"),
        @JsonSubTypes.Type(value = CustomerMessage.ListCustomerOrders.class, name = "listCustomerOrders"),
        @JsonSubTypes.Type(value = SysAdminMessage.ListCustomer.class, name = "listCustomer"),
        @JsonSubTypes.Type(value = SysAdminMessage.ListVendor.class, name = "listVendor")
    })
    public static class ProcurementMessage implements Serializable, Traceable{
    	private static final long serialVersionUID = 3964177533750598971L;
    	@JsonProperty("id") private String id;

		@Override
		public String getId() {
			return this.id;
		}

		@Override
		public void setId(String id) {
			this.id = id;
		}

		public static String procurementMessageToJson(ProcurementMessage message) throws JsonProcessingException{
			 ObjectMapper mapper = new ObjectMapper();
			 return mapper.writeValueAsString(message);
		}

		@Override
		public String toString() {
			return "ProcurementMessage [id=" + id + "]";
		}

    }

	public static enum RequestStatus {
		SUCCESS,FAIL,SAME_STATE,ADD_MEMBER,REMOVE_MEMBER;
	}
	public static enum ClusterMemberEvent {
		ADD_MEMBER,REMOVE_MEMBER,ADD_ADMIN,ADD_DAEMON;

	}
	
	public static class MemberStatus implements Serializable {
		private static final long serialVersionUID = 7339295329106703386L;
		private final Address address;
		private final ClusterMemberEvent event;

		public MemberStatus(Address address, ClusterMemberEvent event) {
			this.address = address;
			this.event = event;
		}

		public Address getAddress() {
			return address;
		}
		public ClusterMemberEvent getClusterMemberEvent() {
			return event;
		}
	}

	public static class Identity implements Serializable {

		private static final long serialVersionUID = 7339295071106709386L;
		private final String vendorId;
		private final Address address;

		public Identity(String vendorId,Address address) {
			this.vendorId = vendorId;
			this.address = address;
		}

		public String getVendorId() {
			return vendorId;
		}

		public Address getAddress(){
			return this.address;
		}

		@Override
		public String toString() {
			return "Identity [vendorId=" + vendorId + ", address=" + address + "]";
		}

	}

	public static class Identify implements Serializable {
		private static final long serialVersionUID = 7349295071106709386L;
		private final Address address;

		public Identify (Address address){
			this.address = address;
		}
		public Address getAddress(){
			return this.address;
		}
		@Override
		public String toString() {
			return "Identify [address=" + address + "]";
		}
	}

	public static class QueueMessage implements Serializable{

		private static final long serialVersionUID = 8363130579029615484L;
		private final String message;
		private final String queue;
		private final String id;
		public QueueMessage(String message, String id, String queue){
			this.message = message;
			this.queue = queue;
			this.id = id;
		}
		public String getMessage() {
			return message;
		}
		public String getQueue() {
			return queue;
		}
		public String getId() {
			return id;
		}
		@Override
		public String toString() {
			return "QueueMessage [message=" + message + ", queue=" + queue + ", id=" + id + ", toString()="
					+ super.toString() + "]";
		}
	}
	
	public static class VendorBidOffer implements Serializable{

		private static final long serialVersionUID = 8502113879145890876L;
		private final String vendorId;
		private final Product product;
		private final String bidOfferId;
		private final String customerId;
		public VendorBidOffer(String vendorId, Product product, String bidOfferId, String customerId) {
			this.vendorId = vendorId;
			this.product = product;
			this.bidOfferId = bidOfferId;
			this.customerId = customerId;
		}
		public String getVendorId() {
			return vendorId;
		}
		public Product getProduct() {
			return product;
		}
		public String getBidOfferId() {
			return bidOfferId;
		}	
		public String getCustomerId(){
			return this.customerId;
		}
		@Override
		public String toString() {
			return "VendorBidOffer [vendorId=" + vendorId  + ", bidOfferId=" + bidOfferId
					+ ", customerId=" + customerId + ", product=" + product.toString()+ "]";
		}		
	}
}
// #messages
