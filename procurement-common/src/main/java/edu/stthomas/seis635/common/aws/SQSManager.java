package edu.stthomas.seis635.common.aws;

import java.nio.charset.Charset;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import akka.actor.ActorRef;
import akka.actor.Cancellable;
import akka.actor.UntypedActorWithStash;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import edu.stthomas.seis635.common.constant.Constants;
import edu.stthomas.seis635.common.message.ProcurementMessages;
import edu.stthomas.seis635.common.message.ProcurementMessages.QueueMessage;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

public class SQSManager extends UntypedActorWithStash{
	LoggingAdapter log = Logging.getLogger(getContext().system(), this);
	private AmazonSQS sqs;
	private String openQueueUrl;
	private final ActorRef replyTo;
	private final Cancellable statusTask;
	
	
	public SQSManager(ActorRef replyTo)  {
		this.replyTo = replyTo;
		FiniteDuration interval = Duration.create(10, TimeUnit.SECONDS);
		statusTask = getContext()
				.system()
				.scheduler()
				.schedule(interval, interval, getSelf(), "heartbeat",
						getContext().dispatcher(), null); 
	}

	@Override
	public void preStart() {
		/*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (~/.aws/credentials).
         */
        AWSCredentials credentials = null;
        try {
            credentials = new ProfileCredentialsProvider().getCredentials();
        } catch (Exception e) {
            log.error(
                    "Cannot load the credentials from the credential profiles file. " +
                    "Please make sure that your credentials file is at the correct " +
                    "location (~/.aws/credentials), and is in valid format. {}",
                    e);
        }

        sqs = new AmazonSQSClient(credentials);
        Region usEast1 = Region.getRegion(Regions.US_EAST_1);
        sqs.setRegion(usEast1);
        
        for (String queueUrl : sqs.listQueues().getQueueUrls()) {
        	log.info("Found QueueUrl: {}",queueUrl);
            if(queueUrl.contains(Constants.REQUEST)){
            	this.openQueueUrl = queueUrl;
            }
        }
	}
	
	@Override
	public void postStop() {
		statusTask.cancel();
	}
	
	
	@Override
	public void onReceive(Object message) throws Exception {
		if (message.equals("heartbeat")) {
			//display queue status
			log.info("SQS heartbeat");
		}else if(message instanceof QueueMessage){
			QueueMessage msg = (QueueMessage)message;
			String queueUrl = getQueueUrlByName(msg.getQueue());
			log.info("Adding message {} to queue at {}",msg.getMessage(),queueUrl);
			sqs.sendMessage(new SendMessageRequest(queueUrl,msg.getMessage()));
		}else if(message.equals(SQSMessages.SQS.POLL)){
			log.info("Reading message from SQS Queue");
			ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(openQueueUrl);
			List<Message> m = sqs.receiveMessage(receiveMessageRequest).getMessages();
			
			if(m.size() > 0){
				Message sqsMessage = m.get(0);
				String messageReceiptHandle = sqsMessage.getReceiptHandle();
				log.info("loading messageId {} for processing...",sqsMessage.getMessageId());
				log.info("message body: \n{}",sqsMessage.getBody());
				byte[] jsonMessage = sqsMessage.getBody().getBytes(Charset.forName("UTF-8"));
				try {
					ProcurementMessages task = new ObjectMapper().reader()
			                .forType(ProcurementMessages.class)
			                .readValue(jsonMessage);
					if(task.message.getId() == null || task.message.getId().isEmpty())
						task.message.setId(sqsMessage.getMessageId());
					log.info("message is of type {}",task.message.getClass().getName());
					replyTo.tell(task.message, ActorRef.noSender());
				}catch(JsonParseException e){
					log.error("Could not parse json {}",sqsMessage.getBody());
					log.error(e.getMessage());
					log.error("Message will be deleted from queue____");
				}
				catch(Exception e){
					log.error("Message: {}",sqsMessage.getBody());
					log.error("Could not parse message. Please ensure that there is an object wrapper for this message");
					log.error(e.getMessage());
					log.error("Message will be deleted from queue_____");
				}
				
				//delete message after processing... or save
				log.info("Delete message from EC2Messages.EC2Oasis");
				sqs.deleteMessage(new DeleteMessageRequest(openQueueUrl, messageReceiptHandle));
			}else {
				log.info("{} queue is empty",Constants.REQUEST);
			}
			
		}else{
			log.error("message {} is unhandled in {}", message,this.getClass().getName());
		}

	}
	
	public String getQueueUrlByName(String queueName){
		for (String queueUrl : sqs.listQueues().getQueueUrls()) {
        	log.info("Lookup queue url for: {}",queueName);
            if(queueUrl.contains(queueName)){
            	log.info("Found queue url {} for queue with name {}",queueUrl,queueName);
            	return queueUrl;
            }
        }
		return null;
	}
	
	
}
