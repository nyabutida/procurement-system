package edu.stthomas.seis635.common.message;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;
//immutable message objects for the customer
public abstract class SysAdminMessage extends ProcurementMessages implements Serializable{

	private static final long serialVersionUID = -7504339923475705387L;	
	
	@Override
	public String toString() {
		return "SysAdminMessage [toString()=" + super.toString() + "]";
	}


	@JsonTypeName("listVendor")
	public static class ListVendor extends SysAdminMessage implements Serializable {

		private static final long serialVersionUID = -9054585999154532395L;
		@JsonProperty("vendorId") private String vendorId;
		
	}
	
	@JsonTypeName("listCustomer")
	public static class ListCustomer extends SysAdminMessage implements Serializable {

		private static final long serialVersionUID = -9154585999154532385L;
		@JsonProperty("customerId") private String customerId;
		
		public String getCustomerId() {
			return customerId;
		}
		
	}
}
