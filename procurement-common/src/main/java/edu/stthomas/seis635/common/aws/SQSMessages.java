package edu.stthomas.seis635.common.aws;

import edu.stthomas.seis635.common.message.ProcurementMessages;

public abstract class SQSMessages extends ProcurementMessages {
	private static final long serialVersionUID = -567878648567184890L;
	
	@Override
	public String toString() {
		return "EC2Messages [toString()=" + super.toString() + "]";
	}
	
	public static enum SQS {
		POLL, ADD; 
	}	
}
