package edu.stthomas.seis635.common.message;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

import edu.stthomas.seis635.common.message.ProcurementMessages.ProcurementMessage;
//immutable message objects for the customer
public abstract class CustomerMessage extends ProcurementMessage implements Serializable{

	private static final long serialVersionUID = -7504339923475705386L;
	@JsonProperty("customerId") private String customerId;
	
	
	public String getCustomerId() {
		return customerId;
	}
	
	@Override
	public String toString() {
		return "CustomerMessage [customerId=" + customerId + ", toString()=" + super.toString() + "]";
	}


	@JsonTypeName("addCustomer")
	public static class AddCustomer extends CustomerMessage implements Serializable {

		private static final long serialVersionUID = -9054585999154532365L;
		@JsonProperty("firstName") private String firstName;
		@JsonProperty("lastName") private String lastName;
		@JsonProperty("address") private String address;
		
		public String getFirstName() {
			return firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public String getAddress() {
			return address;
		}
		@Override
		public String toString() {
			return "AddCustomer [firstName=" + firstName + ", lastName=" + lastName + ", address=" + address
					+ ", toString()=" + super.toString() + "]";
		}		
	}
	
	@JsonTypeName("getBid")
	public static class GetBid extends CustomerMessage implements Serializable {

		private static final long serialVersionUID = -9054585999154532385L;
		@JsonProperty("productId") private String productId;
		@JsonProperty("minNumberOfBids") private int minNumberOfBids;
		@JsonProperty("sortBy") private String sortBy;
		
		public String getProductId() {
			return productId;
		}
		public int getMinNumberOfBids() {
			return minNumberOfBids;
		}
		public String getSortBy() {
			return sortBy;
		}
		@Override
		public String toString() {
			return "GetBid [productId=" + productId + ", minNumberOfBids=" + minNumberOfBids + ", sortBy=" + sortBy
					+ ", toString()=" + super.toString() + "]";
		}		
	}
	
	@JsonTypeName("acceptBid")
	public static class AcceptBid extends CustomerMessage implements Serializable {
		
		private static final long serialVersionUID = -9054584999154532385L;
		@JsonProperty("bidOfferId") private String bidOfferId;
		@JsonProperty("vendorId") private String vendorId;
		
		public String getBidOfferId() {
			return bidOfferId;
		}
		public String getVendorId() {
			return vendorId;
		}
		@Override
		public String toString() {
			return "AcceptBid [bidOfferId=" + bidOfferId + ", vendorId=" + vendorId + ", toString()=" + super.toString()
					+ "]";
		}
	}
	
	@JsonTypeName("listCustomerOrders")
	public static class ListCustomerOrders extends CustomerMessage implements Serializable{

		private static final long serialVersionUID = -5946127388906305765L;
		
		
	}
	
	public static class CustomerOrdersResult implements Serializable{

		private static final long serialVersionUID = 8853055603383888073L;
		
	}
}
