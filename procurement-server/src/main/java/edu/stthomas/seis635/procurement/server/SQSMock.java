package edu.stthomas.seis635.procurement.server;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;

import akka.actor.ActorRef;
import akka.actor.UntypedActor;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import edu.stthomas.seis635.common.aws.SQSMessages;
import edu.stthomas.seis635.common.constant.Constants;
import edu.stthomas.seis635.common.message.ProcurementMessages;

public class SQSMock extends UntypedActor {

	LoggingAdapter log = Logging.getLogger(getContext().system(), this);
	// private final Cancellable timer;
	private List<String> queue;
	private int numMsgRead = 0;
	private final ActorRef replyTo;

	 public SQSMock(ActorRef replyTo) {
		 this.replyTo = replyTo;
	 }

	@Override
	public void preStart() throws Exception {
		queue = new ArrayList<>();
		queue.add(
				"{\n\t\"message\": {\n\t\t\"type\": \"addProduct\",\n\t\t\"vendorId\": \"001\",\n\t\t\"name\": \"Heavy Duty Carpet Brush\",\n\t\t\"productId\":\"ACC_201_BRUSH_HD\",\n\t\t\"description\": \"The Heavy Duty Carpet Brush with Drill Attachment can be used to clean carpet, fabrics, upholstery, seats, wheels and much more.\",\n\t\t\"condition\": \"new\",\n\t\t\"price\": \"19.95\",\n\t\t\"timeToShipInDays\": \"2\"\n\t}\n}");
		queue.add("{\n\t\"message\": {\n\t\t\"type\": \"listInventory\",\n\t\t\"vendorId\": \"001\"\n\t}\n}");
	}

	// @Override
	// public void postStop() {
	// timer.cancel();
	// }

	@Override
	public void onReceive(Object message) throws Exception {
		log.info("{} received message of type {}", this.getClass().getName(), message.getClass().getName());
		if (message.equals(SQSMessages.SQS.POLL)) {
			if (!(numMsgRead > (queue.size() - 1))) {
				String rawMessage = receiveMessage();
				log.info("Process {}",rawMessage);
				byte[] jsonMessage = rawMessage.getBytes(Charset.forName("UTF-8"));
				try {
					ProcurementMessages task = new ObjectMapper().reader().forType(ProcurementMessages.class)
							.readValue(jsonMessage);
					if (task.message.getId() == null || task.message.getId().isEmpty())
						task.message.setId(System.currentTimeMillis() + "");
					log.info("message is of type {}", task.message.getClass().getName());
					replyTo.tell(task.message, ActorRef.noSender());
				} catch (JsonParseException e) {
					log.error("Could not parse json {}", rawMessage);
					log.error(e.getMessage());
					log.error("Message will be deleted from queue____");
				} catch (Exception e) {
					log.error("Message: {}", rawMessage);
					log.error(
							"Could not parse message. Please ensure that there is an object wrapper for this message");
					log.error(e.getMessage());
					log.error("Message will be deleted from queue");
				}

				// delete message after processing... or save
				log.info("Delete message from EC2Messages.EC2Oasis");
			}else {
				log.info("{} queue is empty", Constants.REQUEST);
			}
		} else {
			log.error("message {} is unhandled in {}", message, this.getClass().getName());
		}

	}

	private String receiveMessage() {
		numMsgRead += 1;
		return queue.get(numMsgRead - 1);
	}

}
