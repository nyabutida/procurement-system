package edu.stthomas.seis635.procurement.server;

import java.io.IOException;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import akka.actor.ActorRef;
import akka.actor.Address;
import akka.actor.Cancellable;
import akka.actor.PoisonPill;
import akka.actor.Props;
import akka.actor.UntypedActor;
import akka.cluster.Cluster;
import akka.cluster.ClusterEvent.CurrentClusterState;
import akka.cluster.ClusterEvent.MemberEvent;
import akka.cluster.ClusterEvent.MemberUp;
import akka.cluster.ClusterEvent.ReachabilityEvent;
import akka.cluster.ClusterEvent.ReachableMember;
import akka.cluster.ClusterEvent.UnreachableMember;
import akka.cluster.Member;
import akka.cluster.MemberStatus;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import edu.stthomas.seis635.common.aws.SQSManager;
import edu.stthomas.seis635.common.aws.SQSMessages;
import edu.stthomas.seis635.common.message.CustomerMessage;
import edu.stthomas.seis635.common.message.ProcurementMessages;
import edu.stthomas.seis635.common.message.ProcurementMessages.ClusterMemberEvent;
import edu.stthomas.seis635.common.message.VendorMessage;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;

public class ProcurementServer extends UntypedActor {
	LoggingAdapter log = Logging.getLogger(getContext().system(), this);
	private final String servicePath;
	private final ActorRef systemManager; 
	private final ActorRef sqs; 
	private final Cancellable heartbeatTask;
	private final Set<Address> nodes = new HashSet<Address>();
	private final Set<Address> admins = new HashSet<Address>();

	Cluster cluster = Cluster.get(getContext().system());
	public ProcurementServer(String servicePath, Properties properties) {
		this.servicePath = servicePath;
		this.systemManager = getContext().actorOf(
				Props.create(ProcurementSystemManager.class,servicePath));
		if(properties.getProperty("enable.sqs").equalsIgnoreCase("true"))
			this.sqs = getContext().actorOf(
				Props.create(SQSManager.class,getSelf()));
		else {
			log.info("SQS is not enabled. Mock SQS using pre-loaded messages");
			this.sqs= getContext().actorOf(
					Props.create(SQSMock.class,getSelf()));
		}
		FiniteDuration interval = Duration.create(5, TimeUnit.SECONDS);
		FiniteDuration wait = Duration.create(10, TimeUnit.SECONDS);
		heartbeatTask = getContext()
				.system()
				.scheduler()
				.schedule(wait, interval, getSelf(), "heartbeat",
						getContext().dispatcher(), null); 
	}

	// subscribe to cluster changes, MemberEvent
	@Override
	public void preStart() {
		cluster.subscribe(getSelf(), MemberEvent.class, ReachabilityEvent.class);
	}

	// re-subscribe when restart
	@Override
	public void postStop() {
		cluster.unsubscribe(getSelf());
		heartbeatTask.cancel();
	}

	@Override
	public void onReceive(Object message) throws IOException {
		if(message.equals("heartbeat"))
			gather();
		//log message
		if(!message.equals("heartbeat"))
			log.info("Client received message of type {}",message.getClass().getName());
		
		//consume
		if (message.equals("heartbeat") && !nodes.isEmpty()) {
			sqs.tell(SQSMessages.SQS.POLL, ActorRef.noSender());
		} else if (message instanceof VendorMessage) {
			systemManager.tell(message, getSelf());
		} else if (message instanceof CustomerMessage) {
			systemManager.tell(message, getSelf());
		} else if (message instanceof CurrentClusterState) {
			CurrentClusterState state = (CurrentClusterState) message;
			nodes.clear();
			for (Member member : state.getMembers()) {
				if (member.hasRole("procurementService")
						&& member.status().equals(MemberStatus.up())) {
					nodes.add(member.address());
					log.info("Sending identity message to member at {} "+member.address() + servicePath);
					getContext().actorSelection(
							member.address() + servicePath).tell(
							new ProcurementMessages.Identify(member.address()),
							systemManager);
				}else if (member.hasRole("admin")
						&& member.status().equals(MemberStatus.up())) {
					admins.add(member.address());
					systemManager.tell(new ProcurementMessages.MemberStatus(member.address()
							, ClusterMemberEvent.ADD_ADMIN),getSelf());
				}
			}

		} else if (message instanceof MemberUp) {
			MemberUp mUp = (MemberUp) message;
			if (mUp.member().hasRole("procurementService")) {
				log.info("Member Up with role procurementService at address {}", mUp.member().address());
				nodes.add(mUp.member().address());
				log.info("Sending identity message to member at {} "+mUp.member().address() + servicePath);
				getContext().actorSelection(
						mUp.member().address() + servicePath).tell(
						new ProcurementMessages.Identify(mUp.member().address()),
						systemManager);
			}else if (mUp.member().hasRole("admin")) {
				log.info("Member Up with role admin at address {}", mUp.member().address());
				admins.add(mUp.member().address());
				systemManager.tell(new ProcurementMessages.MemberStatus(mUp.member().address()
						, ClusterMemberEvent.ADD_ADMIN),getSelf());
			}

		} else if (message instanceof MemberEvent) {
			MemberEvent other = (MemberEvent) message;
			nodes.remove(other.member().address());
			admins.remove(other.member().address());
			systemManager
					.tell(new ProcurementMessages.MemberStatus(other.member()
							.address(), ClusterMemberEvent.REMOVE_MEMBER), ActorRef
							.noSender());

		} else if (message instanceof UnreachableMember) {
			UnreachableMember unreachable = (UnreachableMember) message;
			nodes.remove(unreachable.member().address());
			admins.remove(unreachable.member().address());
			systemManager.tell(new ProcurementMessages.MemberStatus(unreachable
					.member().address(), ClusterMemberEvent.REMOVE_MEMBER), ActorRef
					.noSender());

		} else if (message instanceof ReachableMember) {
			ReachableMember reachable = (ReachableMember) message;
			if (reachable.member().hasRole("procurementService")) {
				log.info("ReachableMember with role procurementService at address {}", reachable.member()
						.address());
				nodes.add(reachable.member().address());
				getContext().actorSelection(
						reachable.member().address() + servicePath)
						.tell(new ProcurementMessages.Identify(reachable.member()
								.address()), systemManager);
			}else if (reachable.member().hasRole("admin")) {
				log.info("ReachableMember with role admin at address {}", reachable.member()
						.address());
				admins.add(reachable.member().address());
				systemManager.tell(new ProcurementMessages.MemberStatus(reachable
						.member().address(), ClusterMemberEvent.ADD_ADMIN),
						getSelf());
			}

		} else {
			unhandled(message);
		}
	}

	private void gather() {
		log.info("The procurement System has #{} active admin[s]",admins.size());
		if(admins.size()==0){
			log.info("Something went wrong... There were 0 active procurement system admins in the cluster. "
					+ "In order to avoid Network Partitioning the system will shut down. Please make sure that the admin is the first member to join the cluster");
			// the sender of the poinson pill will remain anonymous :)
			getSelf().tell(PoisonPill.getInstance(), ActorRef.noSender());
		}
		
	}
	

}
