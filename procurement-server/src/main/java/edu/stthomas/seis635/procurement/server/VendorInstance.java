package edu.stthomas.seis635.procurement.server;


import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import edu.stthomas.seis635.common.message.ProcurementMessages;

public class VendorInstance {

	private final String vendorId;
	private final Set<ProcurementMessages.Identity> members;
	
	public VendorInstance(String vendorId) {
		this.vendorId = vendorId;
		this.members = new HashSet<ProcurementMessages.Identity>();
	}

	public String getVendorId() {
		return vendorId;
	}

	public Set<ProcurementMessages.Identity> getMembers() {
		return members;
	}

	@Override
	public String toString() {
		return "VendorInstance [vendorId="
				+ vendorId
				+ ", running applications=["
				+ members.stream().map(i -> i.toString())
						.collect(Collectors.joining(", ")) + "]";
	}
	
}
