package edu.stthomas.seis635.procurement.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import akka.actor.ActorSelection;
import akka.actor.Address;
import akka.actor.Cancellable;
import akka.actor.UntypedActorWithStash;
import akka.event.Logging;
import akka.event.LoggingAdapter;
import edu.stthomas.seis635.common.customer.CustomerOrders;
import edu.stthomas.seis635.common.message.CustomerMessage;
import edu.stthomas.seis635.common.message.CustomerMessage.AcceptBid;
import edu.stthomas.seis635.common.message.ProcurementMessages;
import edu.stthomas.seis635.common.message.ProcurementMessages.ClusterMemberEvent;
import edu.stthomas.seis635.common.message.ProcurementMessages.Identity;
import edu.stthomas.seis635.common.message.ProcurementMessages.MemberStatus;
import edu.stthomas.seis635.common.message.VendorMessage;
import edu.stthomas.seis635.common.message.VendorMessage.AddProductResult;
import edu.stthomas.seis635.common.vendor.VendorInventoryList;
import scala.concurrent.duration.Duration;
import scala.concurrent.duration.FiniteDuration;
import scala.concurrent.forkjoin.ThreadLocalRandom;

public class ProcurementSystemManager extends UntypedActorWithStash {
	LoggingAdapter log = Logging.getLogger(getContext().system(), this);

	private final String servicePath;
	private final Cancellable statusTask;
	private final Map<String, VendorInstance> vendorInstances = new HashMap<String, VendorInstance>();
	final Set<Address> admin = new HashSet<Address>();
	final Set<Address> daemons = new HashSet<Address>();
	
	public ProcurementSystemManager(String servicePath){
		this.servicePath  = servicePath;
		FiniteDuration interval = Duration.create(30, TimeUnit.SECONDS);
		FiniteDuration wait = Duration.create(10, TimeUnit.SECONDS);
		statusTask = getContext()
				.system()
				.scheduler()
				.schedule(wait, interval, getSelf(), "heartbeat",
						getContext().dispatcher(), null); 
	}
	
	@Override
	public void postStop() {
		statusTask.cancel();
	}
	
	@Override
	  public void onReceive(Object message) {
		log.info("Procurement System Manager received message of type {} ", message.getClass().getName());
		
		if(processMemberStatus(message)){
			log.info("Member status message has been processed...");
		}else if (message.equals("heartbeat")){
			printNodeManagerStatus();
		}
		else if(message instanceof VendorInventoryList){
			log.info("Displaying Vendor Inventory List");
			log.info("================================");
			log.info(message.toString());
		}else if(message instanceof AddProductResult){
			log.info("Add Product Result");
			log.info("================================");
			log.info(message.toString());
		}else if(message instanceof CustomerOrders){
			log.info("Display Customer Orders");
			log.info("================================");
			log.info(message.toString());
		}else if(message instanceof VendorMessage){
			VendorMessage msg = (VendorMessage)message;
			String vendorId = msg.getVendorId();
			log.info("Lookup Procurement Service for Vendor Id: {}",msg.getVendorId());
			
			VendorInstance vendorInstance = vendorInstances.get(vendorId);
			if(vendorInstance != null){
				log.info("Found vendor instance for vendor id {}",vendorId);
				ActorSelection vendorService = getRamdomVenderServiceInstance(servicePath,vendorInstance.getMembers());
				vendorService.tell(msg, getSelf());
			}
			else 
				log.info("Could not find vendor instance for vendorId {}", vendorId);
			
		}else if(message instanceof AcceptBid){
			AcceptBid msg = (AcceptBid)message;
			String vendorId = msg.getVendorId();
			log.info("Lookup Procurement Service for Vendor Id: {}",msg.getVendorId());
			
			VendorInstance vendorInstance = vendorInstances.get(vendorId);
			if(vendorInstance != null){
				log.info("Found vendor instance for vendor id {}",vendorId);
				ActorSelection vendorService = getRamdomVenderServiceInstance(servicePath,vendorInstance.getMembers());
				vendorService.tell(msg, getSelf());
			}
			else 
				log.info("Could not find vendor instance for vendorId {}", vendorId);
		}else if (message instanceof CustomerMessage){
			getRamdomAdmin(servicePath).tell(message, getSelf());
		} else if(message instanceof ProcurementMessages.Identity){
			addVendorInstanceFromIdentity((Identity) message);
		}else {
			log.info("message {} could not be handled",message);
			unhandled(message);
		}
	}



	private boolean processMemberStatus(Object message){
		boolean isProcessed = false;
		if (message instanceof ProcurementMessages.MemberStatus){
			MemberStatus status = (MemberStatus)message;
			if(status.getClusterMemberEvent().equals(ClusterMemberEvent.REMOVE_MEMBER)){
				removeVendorMemberInstance(status);
				isProcessed = true;
			}
			else if(status.getClusterMemberEvent().equals(ClusterMemberEvent.ADD_ADMIN)){
				addAdmin(status);
				isProcessed = true;
			}
			else if(status.getClusterMemberEvent().equals(ClusterMemberEvent.ADD_DAEMON)){
				addDaemon(status);
				isProcessed = true;
			}
			else {
				isProcessed = false;
			}
		}
		return isProcessed;
	}
	private void addAdmin(MemberStatus status) {
		admin.add(status.getAddress());
		
	}
	private void addDaemon(MemberStatus status) {
		daemons.add(status.getAddress());
		
	}
	

	
	private ActorSelection getRamdomVenderServiceInstance(String servicePath, Set<Identity> vendorMembers){
		List<Identity> identityList = new ArrayList<Identity>(vendorMembers);
		Identity identity = identityList.get(ThreadLocalRandom.current()
				.nextInt(identityList.size()));
		ActorSelection service = getContext().actorSelection(
				identity.getAddress() + servicePath);
		return service;
	}
	
	private ActorSelection getRamdomAdmin(String servicePath){
		List<Address> admins = new ArrayList<Address>(admin);
		Address address = admins.get(ThreadLocalRandom.current()
				.nextInt(admins.size()));
		ActorSelection service = getContext().actorSelection(
				address + servicePath);
		return service;
	}
	
	private void addVendorInstanceFromIdentity(Identity message) {
		ProcurementMessages.Identity member = (ProcurementMessages.Identity) message;
		log.info("{} received a new service instance for Vendor ID: {}", this.getClass().getName(),
				member.getVendorId());
		log.info(member.toString());
		String vendorId = member.getVendorId();
		vendorInstances.computeIfAbsent(vendorId, k -> (new VendorInstance(vendorId))).getMembers().add(member);
	}

	private void removeVendorMemberInstance(MemberStatus status) {
		String addressToRemove = status.getAddress().toString();
		log.info("Removing node with address {}", addressToRemove);
		log.info("Remove {} from admin if found...", addressToRemove);
		admin.remove(admin);
		for (Iterator<Map.Entry<String, VendorInstance>> vendorInstanceItr = vendorInstances.entrySet()
				.iterator(); vendorInstanceItr.hasNext();) {
			Map.Entry<String, VendorInstance> vendorInstanceEntry = vendorInstanceItr.next();
			String vendorId = vendorInstanceEntry.getKey();
			if (vendorInstances.containsKey(vendorId) && vendorInstances.get(vendorId).getMembers().size() > 0) {
				for (Iterator<Identity> members_it = vendorInstances.get(vendorId).getMembers().iterator(); members_it
						.hasNext();) {
					Identity membersEntry = members_it.next();
					if (membersEntry.getAddress().toString().equals(addressToRemove)) {
						log.info("Removing vendor instance with address {} from vendor id {}", addressToRemove,
								vendorId);
						members_it.remove();
					}
				}
				if (vendorInstances.get(vendorId).getMembers().size() == 0) {
					log.info(
							"Vendor with Vendor ID: {} has no active members. Removing vendor from procurement system cluster.",
							vendorId);
					vendorInstances.remove(vendorId);
				}
			}
		}

	}
	
	private void printNodeManagerStatus(){
		log.info("=============VENDORS===============");
		log.info("Number of vendors in the system, {}",vendorInstances.size());
		vendorInstances.forEach((k,v) ->log.info("{} - {}",k,v));
		log.info("===================================");
		
	}

}