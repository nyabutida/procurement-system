package edu.stthomas.seis635.procurement.server;

import static edu.stthomas.seis635.common.injection.SpringExtension.SpringExtProvider;

import java.io.File;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorSystem;

@Configuration
public class ProcurementServerConfiguration {
	private static final Logger logger = LoggerFactory
			.getLogger(ProcurementServerConfiguration.class);
	// the application context is needed to initialize the Akka Spring Extension
	@Autowired
	private ApplicationContext applicationContext;
	@Value("${routerconf}")
	private String router;
	@Value("$[akka.application.conf}")
	private String applicationConf;

	/**
	 * Actor system singleton for this application.
	 */
	@Bean(name = { "procurementServerSystem" })
	public ActorSystem actorSystem() {
		System.out.println("Router Path "+router);
		System.out.println("File Exists "+(new File(router)).exists());
		Config config = ConfigFactory.parseFile(new File(router)).resolve()
				.withFallback(ConfigFactory.parseFile(new File(applicationConf)).resolve());
		logger.info("Initializaing procurement server system...");
		ActorSystem system = ActorSystem.create("ClusterSystem", config);
		logger.info("Setting procurement server system application context...");
		SpringExtProvider.get(system).initialize(applicationContext);
		return system;
	}
}
