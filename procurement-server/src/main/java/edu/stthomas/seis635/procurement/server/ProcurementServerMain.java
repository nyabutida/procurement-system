package edu.stthomas.seis635.procurement.server;


import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import akka.actor.ActorSystem;
import akka.actor.Props;

public class ProcurementServerMain {
	private static final Logger logger = LoggerFactory
			.getLogger(ProcurementServerMain.class);
	private static ApplicationContext ctx;

	public static void main(String[] args) {
		ctx = new ClassPathXmlApplicationContext("application-context.xml");
		logger.info("Initialize Server ActorSystem...");
		ActorSystem system = (ActorSystem) ctx.getBean("procurementServerSystem");
		logger.info("Set client...");
		Properties prop = (Properties)ctx.getBean("applicationConfig");	
		system.actorOf(Props.create(ProcurementServer.class, "/user/procurementservice",prop),
				"server");
	}
}
