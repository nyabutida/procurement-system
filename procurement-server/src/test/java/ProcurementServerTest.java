import static edu.stthomas.seis635.common.injection.SpringExtension.SpringExtProvider;

import java.io.File;
import java.util.Properties;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.actor.Terminated;
import akka.testkit.JavaTestKit;
import edu.stthomas.seis635.procurement.server.ProcurementServer;
 
public class ProcurementServerTest {
//  }
  static ActorSystem system;
  static ApplicationContext ctx;
  
  @BeforeClass
	public static void setup() {
		File router = new File("src/main/resources/workspace/router.conf");
		File applicationConf = new File("src/main/resources/workspace/router.conf");
		Config config = ConfigFactory.parseFile(router).resolve()
				.withFallback(ConfigFactory.parseFile(applicationConf).resolve());
		 system = ActorSystem.create("ClusterSystem", config);
		 ctx = new ClassPathXmlApplicationContext("**test-application-context.xml");
		 SpringExtProvider.get(system).initialize(ctx);
	}
  
  @AfterClass
  public static void teardown() {
    JavaTestKit.shutdownActorSystem(system);
    system = null;
  }
 
  @Test
  public void testProcurementServerTermination() {
	  //if there are no admin servers up the procurement server should terminate immediately
    new JavaTestKit(system) {{
      final Props props = Props.create(ProcurementServer.class,"/user/procurementservice", new Properties());
      final ActorRef subject = system.actorOf(props);
      final JavaTestKit probe = new JavaTestKit(system);
      probe.watch(subject);
      new Within(duration("5 second")) {
    	    public void run() {
    	        probe.expectMsgClass(Terminated.class);
    	    }
    	  };
 
    }};
  }
  
}